<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use Monolog\DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
// use App\Controller\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;
use App\Form\ArticleType;


class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo): Response
    {
        $articles = $repo->findAll();

        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles
        ]);
    }


    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        return $this->render(
            'blog/home.html.twig',
            [
                'title' => "Bienvenue sur le blog de Marino"
            ]
        );
    }

    /**
     * @Route("/blog/new", name="blog_create")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */

    // public function form(Article $article = null, HttpFoundationRequest $request, EntityManagerInterface $manager)
    // {
    //     if (!$article) {
    //         $article = new Article();
    //     }

    //     $article->setTitle("Titre d'exemple")
    //         ->setContent("Le contenu de l'article");

    //     // $form = $this->createFormBuilder($article)
    //     //     ->add('title')
    //     //     ->add('content')
    //     //     ->add('image')
    //     //     ->getForm();

    //     $form = $this->createForm(ArticleType::class, $article);

    //     //Analyse de la requete
    //     $form->handleRequest($request);
    //     //Vérification des champs
    //     if ($form->isSubmitted() && $form->isValid()) {
    //         if ($article->getId()) {
    //             $article->setCreateAt(new \DateTimeImmutable());
    //         }
    //         // $article->setCreateAt(new DateTimeImmutable('now'));
    //         //Envoi de l'article dans le manager
    //         $manager->persist($article);
    //         $manager->flush();

    //         return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
    //     }
    //     // dump($article);

    //     return $this->render('blog/create.html.twig', [
    //         'formArticle' => $form->createView(),
    //         'editMode' => $article->getId() !== null
    //     ]);

    public function create(Request $request, EntityManagerInterface $manager)
    {
        $article = new Article();

        $form = $this->createFormBuilder($article)
            ->add('title')
            ->add('content')
            ->add('image')
            ->getForm();

        $form->handleRequest($request);

        dump($article);

        return $this->render('blog/create.html.twig', [
            'formArticle' => $form->createView()
        ]);
    }


    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show(Article $article)
    {

        return $this->render('blog/show.html.twig', [
            'article' => $article
        ]);
    }
}
